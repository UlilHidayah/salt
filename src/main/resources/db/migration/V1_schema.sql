CREATE TABLE `Konsumens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` char(30) DEFAULT NULL,
  `provinsi` char(30) DEFAULT NULL,
  `tgl_registrasi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;