<!DOCTYPE html><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All Consumers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet"href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
    <script src=
    "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
        </script>
</head>

<body>
    <div class="container my-2">
        <div class="card">
            <div class="card-body">
                <c:if test="${success==1}">
                <div class="alert alert-success" role="alert">
                  Add Consumer Success!
                </div>
                </c:if>
                <c:if test="${success==0}">
                <div class="alert alert-danger" role="alert">
                  Add Consumer Failed!
                </div>
                </c:if>
                <div class="container my-5">
                    <p class="my-5">
                        <a href="/konsumen/add-consumer" class="btn btn-primary">
                            <i class="fas fa-user-plus ml-2">Add Consumer</i>
                        </a>
                    </p>
                    <div class="form-group col-md-8">
                        <label for="name" class="col-form-label">Search the table for All of column : </label>
                        <input type="text" class="form-control" id="search" placeholder="Search here" />
                    </div>
                        <div class="col-md-10">
                        <c:if test="${consumers.size()==0}">
                            <h2>No record found !!</h2>
                        </c:if>
                        <c:if test="${consumers.size() gt 0 }">
                            <div>
                                <table class="table table-striped table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Kota</th>
                                            <th>Provinsi</th>
                                            <th>Tanggal Registrasi</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="datas">
                                        <c:forEach var="consumer" items="${consumers}">
                                            <tr>
                                                <td>${consumer.nama}</td>
                                                <td>${consumer.alamat}</td>
                                                <td>${consumer.kota}</td>
                                                <td>${consumer.provinsi}</td>
                                                <td>${consumer.tgl_registrasi}</td>
                                                <td>${consumer.status == "1" ? "Aktif" : "Non-Aktif"}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        $("#search").on("keyup", function() {
                                            var value = $(this).val().toLowerCase();
                                            $("#datas tr").filter(function() {
                                                $(this).toggle($(this).text()
                                                .toLowerCase().indexOf(value) > -1)
                                            });
                                        });
                                    });
                                </script>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>