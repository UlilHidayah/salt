<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Add Student</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
</head>

<body>
    <div class="container my-5">
        <h3>Add Student</h3>
        <div class="card">
            <div class="card-body">
                <div class="col-md-10">
                    <form:form action="/konsumen/save-consumer"
                        method="post" modelAttribute="command">
                        <form:hidden path="id" />
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="nama" class="col-form-label">Nama</label>
                                <form:input type="text" class="form-control" id="nama"
                                    path="nama" placeholder="" required="true"/>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="alamat" class="col-form-label">Alamat</label>
                                <form:textarea type="text" class="form-control" id="alamat"
                                    path="alamat" placeholder="" required="true"/>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="kota" class="col-form-label">Kota</label>
                                <form:input type="text" class="form-control" id="kota"
                                    path="kota" placeholder="" required="true" value=""/>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="provinsi" class="col-form-label">Provinsi</label>
                                <form:input type="text" class="form-control" id="provinsi"
                                    path="provinsi" placeholder="" required="true" value=""/>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="provinsi" class="col-form-label">Provinsi</label>
                                <select id="status" name="status" required="true">
                                  <option value="1" selected="true">Aktif</option>
                                  <option value="0">Non-Aktif</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-primary" value=" Submit ">
                            </div>

                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>