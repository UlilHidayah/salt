package com.springjava.salt.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Data
public class Konsumen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nama;
    private String alamat;
    @Column(name = "kota", columnDefinition = "char(30)")
    private String kota;
    @Column(name = "provinsi", columnDefinition = "char(30)")
    private String provinsi;
    private Timestamp tgl_registrasi;
    private char status;
}