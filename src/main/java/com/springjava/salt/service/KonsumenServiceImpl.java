package com.springjava.salt.service;

import com.springjava.salt.entity.Konsumen;
import com.springjava.salt.repository.KonsumenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KonsumenServiceImpl implements KonsumenService {
    @Autowired
    KonsumenRepository konsumenRepository;

    @Override
    public void save(Konsumen konsumen) {
        if (konsumen.getId() == null) {
            konsumenRepository.save(konsumen);
        } else {
            Konsumen konsumenUpdate = konsumenRepository.findById(konsumen.getId()).get();
            konsumenUpdate.setNama(konsumen.getNama());
            konsumenUpdate.setKota(konsumen.getKota());
            konsumenUpdate.setAlamat(konsumen.getAlamat());
            konsumenUpdate.setProvinsi(konsumen.getProvinsi());
            konsumenUpdate.setStatus(konsumen.getStatus());

            konsumenRepository.save(konsumenUpdate);
        }
    }

    @Override
    public List<Konsumen> getAll() {
        return konsumenRepository.findAll();
    }

    @Override
    public Konsumen getById(Integer id) {
        return konsumenRepository.findById(id).get();

    }

    @Override
    public void delete(Konsumen konsumen) {
        konsumenRepository.delete(konsumen);
    }
}
