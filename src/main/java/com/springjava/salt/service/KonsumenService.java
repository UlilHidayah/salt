package com.springjava.salt.service;

import com.springjava.salt.entity.Konsumen;

import java.util.List;

public interface KonsumenService {
    void save(Konsumen konsumen);
    List<Konsumen> getAll();
    Konsumen getById(Integer id);
    void delete(Konsumen student);
}
