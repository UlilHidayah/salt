package com.springjava.salt.controller;

import com.springjava.salt.entity.Konsumen;
import com.springjava.salt.service.KonsumenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/konsumen")
public class KonsumenController {

    @Autowired
    KonsumenService konsumenService;

    @GetMapping("/dashboard")
    public ModelAndView getAllStudents() {
        ModelAndView mav = new ModelAndView("konsumen");
        List<Konsumen> konsumenList = konsumenService.getAll();
        mav.addObject("status", "");
        mav.addObject("consumers", konsumenList);
        return mav;
    }

    @GetMapping("/add-consumer")
    public ModelAndView addConsumer() {
        ModelAndView mav = new ModelAndView("add_consumer");
        mav.addObject("command", new Konsumen());
        return mav;
    }

    @PostMapping("/save-consumer")
    public String saveConsumer(@ModelAttribute Konsumen konsumen) {
        konsumen.setTgl_registrasi(new Timestamp(new Date().getTime()));
        konsumenService.save(konsumen);
        return "redirect:/konsumen/dashboard";
    }

    @GetMapping("/student-consumer/{id}")
    public ModelAndView getConsumer(@PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("add_consumer");
        Konsumen konsumen = konsumenService.getById(id);
        mav.addObject("command", konsumen);
        return mav;
    }

    @GetMapping("/consumer-delete/{id}")
    public String deleteConsumer(@PathVariable("id") Integer id) {
        Konsumen deleteConsumer = konsumenService.getById(id);
        konsumenService.delete(deleteConsumer);
        return "redirect:/konsumen/dashboard";
    }
}
