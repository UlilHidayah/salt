package com.springjava.salt.repository;

import com.springjava.salt.entity.Konsumen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KonsumenRepository extends JpaRepository<Konsumen, Integer> {

}
